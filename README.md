# OpenML dataset: analcatdata_dmft

https://www.openml.org/d/469

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Unknown   
**Source**: [Jeffrey S. Simonoff](http://people.stern.nyu.edu/jsimonof/AnalCatData/Data/) - 2003    
**Please cite**: Jeffrey S. Simonoff, Analyzing Categorical Data, Springer-Verlag, 2003

One of the datasets used in the book "Analyzing Categorical Data,"
by Jeffrey S. Simonoff. It contains data on the DMFT Index (Decayed, Missing, and Filled Teeth) before and after different prevention strategies. The prevention strategy is commonly used as the (categorical) target.

### Attribute information  
* DMFT.Begin and DMFT.End: DMFT index before and after the prevention strategy
* Gender of the individual
* Ethnicity of the individual

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/469) of an [OpenML dataset](https://www.openml.org/d/469). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/469/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/469/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/469/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

